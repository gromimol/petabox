$(document).ready(function() {
    $('.js--form').on('click', function(e) {
        e.preventDefault()
        $('body').addClass('noscroll');
        $('#overlay').show();
        $('.confirm-popup').addClass('active');
    })

    $('.close-popup, .confirm-popup .btn, #overlay').on('click', function(e) {
        e.preventDefault()
        $('body').removeClass('noscroll');
        $('#overlay').hide();
        $('.confirm-popup').removeClass('active');
    })
})